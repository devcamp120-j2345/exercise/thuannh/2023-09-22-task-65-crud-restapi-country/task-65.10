package com.devcamp.countryregionjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CountryregionjpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CountryregionjpaApplication.class, args);
	}

}
